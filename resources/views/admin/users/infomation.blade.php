@extends('admin.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{ asset('adminlte\style\user\select2.min.css') }}">
@endsection
@section('js')
    <script src="{{ asset('adminlte\style\user\user.js') }}"></script>
    <script src="{{ asset('adminlte\style\user\select2.min.js') }}"></script>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('users.index') }}">Danh sách hân viên</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety info">
        @include('admin.layouts.alert')
        <div class="">
            <div class="card-body center">
                <h4 class="card-title"> Nhân viên : <b>{{ $user->name }}</b></h4>
                <h6 class="card-subtitle mb-2 text-muted"> &ensp;({{ $user->user }} - {{ $user->department->name }})</h6>
            </div>
        </div>
        
        <div><b><u>Danh sách sản phẩm đang mượn</u></b></div>
        <hr>
        @if ($user->requsets->where('status', requestSolved)->whereIn('action', [requestExported, requestReturn])->count() > 0)
            <table class="table table-light table-hover">
                <thead class="thead-light">
                    <tr>
                        <th width=5%>STT</th>
                        <th>Tên sản phẩm</th>
                        <th>Mã sản phẩm</th>
                        <th>Ngày mượn</th>
                        <th>Ngày trả</th>
                        <th>Người xác nhận</th>
                        <th>Tình trạng</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user->requsets->where('status', requestSolved)->whereIn('action', [requestExported, requestReturn]) as $key => $request)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $request->product->name }}</td>
                            <td>{{ $request->product->code }}</td>
                            <td>{{ $request->brorrowed_date }}</td>
                            <td>{{ $request->date_off }}</td>
                            <td>{{ $request->history->user->name ?? '' }}</td>
                            <td>
                                <span><a class="badge button badge-outline-dark" href="?action={{ requestExported }}">Đã
                                        xuất sản phẩm </a></span>
                            <td>
                                @if ($request->action == requestReturn)
                                    <span><a class="badge button badge-outline-dark"
                                            href="#">Đang yêu cầu trả</a></span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <div style="text-align:center">Không tìm thấy yêu cầu nào...</div>
        @endif
    </div>
@endsection
