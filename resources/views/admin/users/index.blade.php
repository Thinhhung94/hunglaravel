@extends('admin.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{ asset('adminlte\style\user\select2.min.css') }}">
@endsection
@section('js')
    <script src="{{ asset('adminlte\style\user\user.js') }}"></script>
    <script src="{{ asset('adminlte\style\user\select2.min.js') }}"></script>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('users.index') }}">Danh sách tài khoản</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @can('user.create')
            <a href="{{ route('users.create') }}" class="btn button btn-sm btn-primary" style="float: right">Thêm tài khoản</a>
        @endcan
        @include('admin.layouts.alert')
        <table class="table table-light table-hover">
            <thead class="thead-light">
                <tr>
                    <th width=5%>STT</th>
                    <th>Tên Thành viên</th>
                    <th>User</th>
                    <th>Giới tính </th>
                    <th>Phone</th>
                    <th>Địa chỉ</th>
                    <th>Phòng ban</th>
                    <th>Loại tài khoản</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $key => $user)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->user }}</td>
                        <td>{{ $user->gender == genderMafe ? 'Nam' : 'Nữ' }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ $user->address }}</td>
                        <td><a href="?department={{ $user->department_id }}">{{ $user->department->name ?? '' }}</a></td>
                        <td>{{ $user->type == typeUser ? 'User' : 'Hệ thống' }}</td>
                        <td>
                            <div class="row">
                            
                                @can('product.update')
                                <a href="{{ route('users.edit', $user->id) }}" class="btn button btn-warning"><i
                                    class="fa fa-edit"></i></a> &ensp;
                                @endcan
                                @can('product.delete')
                                    @if (Auth::id() != $user->id)
                                        <form action="{{ route('users.destroy', $user->id) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn button btn-danger"
                                                onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-trash"></i></button>
                                        </form>
                                    @endif
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div>{!! $users->appends(request()->all())->links() !!}</div>
    </div>
@endsection
