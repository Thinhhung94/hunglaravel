@extends('admin.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{ asset('adminlte\style\user\select2.min.css') }}">
@endsection
@section('js')
    <script src="{{ asset('adminlte\style\user\user.js') }}"></script>
    <script src="{{ asset('adminlte\style\user\select2.min.js') }}"></script>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('users.index') }}">Danh sách tài khoản</a></li>
    <li class="breadcrumb-item active">Tạo tài khoản </li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @include('admin.layouts.alert')
        @include('admin.users.form', [
            'lable' => 'Phòng ban',
            'department' => $departments,
            'button' => 'Thêm thành viên',
            'route' => route('users.store'),
        ])
    </div>
@endsection
