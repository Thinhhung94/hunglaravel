@extends('admin.layouts.main')
@section('css')
    <style>
        p {
            color: black;
        }

        .infomation {
            padding: 1em 0.5em;
        }

        .image {
            width: 350px;
            height: 265px;
            border: 1px solid burlywood;
            box-shadow: rgb(50 50 93 / 25%) 0px 50px 100px -20px, rgb(0 0 0 / 30%) 0px 30px 60px -30px, 
                rgb(10 37 64 / 35%) 0px -2px 6px 0px inset;
            /* box-shadow: rgb(0 0 0 / 7%) 0px 1px 2px, rgb(0 0 0 / 7%) 0px 2px 4px, rgb(0 0 0 / 7%) 0px 4px 8px, rgb(0 0 0 / 7%) 0px 8px 16px, rgb(0 0 0 / 7%) 0px 16px 32px, rgb(0 0 0 / 7%) 0px 32px 64px; */
        }

        .image img {
            width: 350px;
            height: 265px;
            object-fit: cover;
            box-shadow: rgb(50 50 93 / 25%) 0px 50px 100px -20px, rgb(0 0 0 / 30%) 0px 30px 60px -30px, rgb(10 37 64 / 35%) 0px -2px 6px 0px inset;
        }
    </style>
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('user.view.product') }}">Danh sách sản phẩm</a></li>
    <li class="breadcrumb-item"><a href="#">Chi tiết sản phẩm</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        <div class="row">
            <div class="col-6">
                <div class="image">
                    <img width="100%" src="{{ asset('uploads/product/' . $product->image) }}" alt="">
                </div>
            </div>
            <div class="col-6">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <button class="nav-link active" id="nav-home-tab" data-toggle="tab" data-target="#nav-home"
                            type="button" role="tab" aria-controls="nav-home" aria-selected="true">Sản
                            phẩm</button>
                        <button class="nav-link" id="nav-profile-tab" data-toggle="tab" data-target="#nav-profile"
                            type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Ghi
                            chú</button>

                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <br>

                        <p>Tên sản phẩm : <b>{{ $product->name }}</b></p>
                        <p>Mã sản phẩm : <b>{{ $product->code }}</b></p>
                        <p>Danh mục sản phẩm : <b>{{ $product->category->name }}</b></p>
                        <p>Tình trạng : <span
                                class="btn btn-sm btn-{{ $product->status == warehouse ? 'success' : 'danger' }}">{{ $product->status == warehouse ? 'Trong kho' : 'Đã mượn' }}</span>
                        </p>
                        @if ($product->status == warehouse)
                            <p>
                                <span><a class=" badge btn-outline-primary badge-outline-primary"
                                        href="{{ route('requests.create') . "?slug=$product->slug" }}">Mượn ngay</a></span>
                            </p>
                        @endif

                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <br>
                        <p>Ghi chú : {{ $product->note }}</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
