<div class="form-group">
    <label for="" class="card-title" >{{ $lable ?? "" }}</label>
    <select class="{{ $class ?? "form-control" }}" name="{{ $name ?? "" }}" id="" >
        @foreach ( $data as $item)
            <option value="{{ $item->id }}" {{isset($id) && $id == $item->id ? "selected" : "" }}>{{ $item->name }}</option>
        @endforeach
     </select>
     @error($name)
     <span class=" text-danger">{{ $message }}</span>
 @enderror
</div>