<form action="{{ $route ?? route('requests.update', $request->id) }}" method="Post" enctype="multipart/form-data">
    @csrf
    @method($method ?? 'post')
    <div class="form-group">

        @include('admin.components.input', [
            'name' => 'id',
            'type' => 'hidden',
            'value' => $request->id ?? '',
        ])

        @include('admin.components.input', [
            'name' => 'product_id',
            'type' => 'hidden',
            'value' => $product->id ?? '',
        ])

        @include('admin.components.label_form', [
            'label' => 'Tên sản phẩm',
            'name' => 'name',
            'atribute' => 'readonly',
            'value' => $product->name ?? old('code'),
        ])

        @include('admin.components.label_form', [
            'label' => 'Mã sản phẩm',
            'name' => 'code',
            'atribute' => 'readonly',
            'value' => $product->code ?? old('code'),
        ])
        @include('admin.components.label_form', [
            'label' => 'Ngày mượn',
            'name' => 'brorrowed_date',
            'type' => 'date',
            'value' => $request->brorrowed_date ?? old('brorrowed_date'),
        ])

        @include('admin.components.label_form', [
            'label' => 'Ngày Trả',
            'name' => 'date_off',
            'type' => 'date',
            'value' => $request->date_off ?? old('date_off'),
        ])



        <div class="form-group">
            <button type="submit" class="btn btn-sm btn-primary">{{ $button }}</button>
        </div>
    </div>
</form>
