@extends('admin.layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{ route('user.list.request') }}">Danh sách yêu cầu</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @include('admin.layouts.alert')
        @if ($listRequsets->count() > 0)
            <table class="table table-light table-hover">
                <thead class="thead-light">
                    <tr>
                        <th width=5%>STT</th>
                        <th>Tên sản phẩm</th>
                        <th>Mã sản phẩm</th>
                        <th>Ngày mượn</th>
                        <th>Ngày trả</th>
                        <th>Tình trạng</th>
                        <th width = "7%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($listRequsets as $key => $request)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $request->product->name }}</td>
                            <td>{{ $request->product->code }}</td>
                            <td>{{ $request->brorrowed_date }}</td>
                            <td>{{ $request->date_off }}</td>
                            <td>
                                @if ($request->status == requestPending)
                                    <span><a class="badge badge-secondary" href="?status={{ requestPending }}">Đang
                                            chờ</a></span>
                                @elseif($request->status == requestSolved)
                                    @if ($request->action == requestExported || $request->action == requestReturn)
                                        <span><a class="badge badge-outline-dark" href="?status={{ requestSolved }}">Đã
                                                xuất hàng</a></span>
                                    @else
                                        <span><a class="badge badge-success" href="?status={{ requestSolved }}">Đã
                                                duyệt</a></span>
                                    @endif
                                @elseif($request->status == requestRefuse)
                                    <span><a class="badge badge-danger" href="?status={{ requestRefuse }}">Từ
                                            chối</a></span>
                                @elseif ($request->status == returned)
                                    <span><a class="badge badge-dark" href="?status={{ requestReturn }}">Đã trả
                                            hàng</a></span>
                                @endif
                            <td>
                                @if ($request->status == requestPending)
                                    <div class="row">
                                        <a href="{{ route('requests.edit', $request->id) }}"
                                            class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a> &ensp;
                                        <form action="{{ route('delete.request', $request->id) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-sm btn-danger"
                                                onclick="return confirm('Bạn có chắc chắn muốn xóa yêu cầu?')">
                                                <i class="fa fa-trash"></i></button>
                                        </form>
                                    </div>
                                @elseif($request->status == requestSolved)
                                    @if ($request->action == requestExported || $request->action == requestReturn)
                                        <form action="{{ route('request.return') }}" method="post">
                                            @csrf
                                            @method('put')
                                            <input type="hidden" name="id" value="{{ $request->id }}">
                                            <button type="submit"
                                                class="btn btn-sm btn-{{ $request->action == requestExported ? 'outline-' : '' }}primary"
                                                onclick="return confirm('Bạn có chắc chắn yêu cầu trả hàng?')">
                                                <i class="fal fa-undo"></i></button>
                                        </form>
                                    @else
                                        <form action="{{ route('request.export') }}" method="post">
                                            @csrf
                                            @method('put')
                                            <input type="hidden" name="id" value="{{ $request->id }}">
                                            <button type="submit"
                                                class="btn btn-sm btn-{{ $request->action == requestExport ? '' : 'outline-' }}primary"
                                                onclick="return confirm('Bạn có chắc chắn yêu cầu xuất sản phẩm?')">
                                                <i class="fal fa-file-export"></i></button>
                                        </form>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div >{!! $listRequsets->appends(request()->all())->links() !!}</div>
        @else
            <div style="text-align:center">Không tìm thấy yêu cầu nào...</div>
        @endif
    </div>
@endsection
