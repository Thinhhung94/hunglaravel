@extends('admin.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{ asset('adminlte\style\roles\role.css') }}">
@endsection
@section('js')
<script src="{{ asset('adminlte\style\roles\role.js') }}"></script>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{ route('roles.index') }}">Danh sách roles</a></li>
    <li class="breadcrumb-item active"><a href="#">Thêm role</a></li>
@endsection
@section('content')
    {{-- <div class="formAdd row justify-content-center col-12" > --}}
    <div class="col-12 box-bety">
        @include('admin.layouts.alert')
        <form action="{{ route('roles.store') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="name">role Name</label>
                <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control"
                    placeholder="Nhập tên role...">
            </div>
            <div class="col-12 row">
                <div class="col-12">
                    <label for="">
                        <input id="checkall" type="checkbox" class="checkall" id="" value=""> &ensp;
                        <label for="checkall"> Check All</label>
                    </label>
                </div>
                @foreach ($permisstions as $permisstion)
                    <div class="card border-primary mb-3 col-12">
                        <div class="card-header">
                            <div >
                                <input type="checkbox" class="parentPer" id="" value=""> &ensp;
                                <span class="back">Chức năng : &ensp;<b>{{ ucfirst($permisstion->name) }}</b></h5>  
                            </div>
                        </div>
                        <hr>
                        <div class="card-body row">
                            @foreach ($permisstion->childs as $child)
                                <div class="col-3">
                                    <span class="card-title">
                                        <input type="checkbox" class="chilPer" name="child[]" id=""
                                            value="{{ $child->id }}">&ensp;
                                        <label for="">
                                           <small> {{ $child->name }}</small>
                                        </label>
                                    </span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Thêm Role</button>
        </form>
    </div>
    {{-- </div> --}}
@endsection
