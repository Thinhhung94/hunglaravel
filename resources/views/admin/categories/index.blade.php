@extends('admin.layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">List Products</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @can('category.create')
            <a href="{{ route('categories.create') }}" class="btn button btn-sm btn-primary" style="float: right">Thêm Danh mục</a>
        @endcan
        <span class="col-10">
        @include('admin.layouts.alert')</span>
        <table class="table table-light table-hover">
            <thead class="thead-light">
                <tr>
                    <th width=5%>STT</th>
                    <th>Tên danh mục</th>
                    <th style="text-align: center">Số lượng Thực tế</th>
                    <th style="text-align: center">Số lượng Trong kho</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $key => $category)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td><a href="{{ route('products.index') }}?category={{ $category->id }}">{{ $category->name }}</a>
                        </td>
                        <td style="text-align: center">{{ $category->products->count() }}</td>
                        <td style="text-align: center">{{ $category->products()->where('status', warehouse)->count() }}</td>
                        <td>
                            <div class="row">
                                @can('category.update')
                                    <a href="{{ route('categories.edit', $category->id) }}" class="button btn btn-warning"><i
                                            class="fa fa-edit"></i></a> &ensp;
                                @endcan
                                @can('category.delete')
                                    <form action="{{ route('categories.destroy', $category->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="button btn btn-danger" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash"></i></button>
                                    </form>
                                @endcan


                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
