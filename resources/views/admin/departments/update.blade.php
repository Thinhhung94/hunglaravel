@extends('admin.layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('departments.index') }}">Departments</a></li>
    <li class="breadcrumb-item active">Update Deparrtment</li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @include('admin.layouts.alert')
        <form action="{{ route('departments.update', $department->id) }}" method="post">
            @csrf
            @method('put')
            <div class="form-group">
                @include('admin.components.input',[
                    'type' => 'hidden',
                    'name' => 'id',
                    'value' => $department->id
                ])
                @include('admin.components.label_form', [
                    'label' => 'Tên danh mục ',
                    'name' => 'name',
                    'placeholder' => 'Mời bạn nhập tên danh mục ...',
                    'value' => $department->name,
                ])
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Sửa Phòng ban</button>
        </form>
    </div>
@endsection
