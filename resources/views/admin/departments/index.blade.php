@extends('admin.layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('departments.index') }}">Phòng ban</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @can('department.create')
        <a href="{{ route('departments.create') }}" class="btn button btn-sm btn-primary" style="float: right">Thêm phòng ban</a>
        @endcan
        <div class="col-10">@include('admin.layouts.alert')</div>
        <table class="table table-light table-hover">
            <thead class="thead-light">
                <tr>
                    <th width=5%>STT</th>
                    <th>Tên phòng ban</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($departments as $key => $department)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $department->name }}</td>
                        <td>
                            <div class="row">
                                @can('department.update')
                                <a href="{{ route('departments.edit', $department->id) }}" class="button btn btn-warning"><i
                                    class="fa fa-edit"></i></a> &ensp;
                                @endcan
                                @can('department.delete')
                                <form action="{{ route('departments.destroy', $department->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="button btn btn-danger" onclick="return confirm('Are you sure?')">
                                        <i class="fa fa-trash"></i></button>
                                </form>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
@endsection
