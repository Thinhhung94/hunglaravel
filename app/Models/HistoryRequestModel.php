<?php

namespace App\Models;
use Illuminate\Support\Facades\Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryRequestModel extends Model
{
    use HasFactory;
    protected $table = 'history_requests';
    public $timestamps = false;
    protected $fillable = [
        'request_id',
        'admin_id',
        'status',
    ];

    public function  request() 
    {
        return $this->belongsTo(RequestModel::class, 'request_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(UserModel::class, 'admin_id', 'id');
    }

    public function createHistory($request_id, $status)
    {
        static::create([
            'admin_id' => Auth::id(),
            'request_id' => $request_id,
            'status' =>  $status
        ]);
    }
    public function updateStatus($request_id, $status)
    {
        static::where('request_id',$request_id)->update([
            'status' =>  $status
        ]);
    }

}
