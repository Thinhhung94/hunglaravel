<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Helper\Image;

class ProductModel extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'products';
    protected $fillable = [
        'name',
        'slug',
        'category_id',
        'code',
        'status',
        'note',
        'image'
    ];
    const folder = 'product';
    public function category(){
        return $this->belongsTo(CategoryModel::class, 'category_id', 'id');
    }

    public function scopeSearch($query)
    {
        if ($search = request()->search) {
            $query = $query->where('name', 'like', '%' . $search . '%');
        }
        return $query;
    }

    public function scopeCategory($query)
    {
        if(isset(request()->category) && request()->category !=null) {
            $query = $query->where('category_id',request()->category);     
        }
        return $query;
    }

    public function list()
    {
        $products = static::with(['category'])->orderBy('id','desc')->Search()->Category()->paginate(20);
        return $products;
    }

    public function findBySlug($slug)
    {
        $product = static::with(['category'])->where('slug',$slug)->first();
        return $product;
    }

    public function findById($id)
    {
        $product = static::with(['category'])->where('id', $id)->first();
        return $product;
    }

    public function createProduct($request)
    {
        $image = new Image;
        $image = $image->uploadImage($request->image, self::folder , str::slug($request->name));
        static::create([
            'name' => $request->name,
            'slug' => str::slug($request->name),
            'code' => $request->code,
            'category_id' => $request -> category_id,
            'status' => $request ->status,
            'note' => $request ->note,
            'image' => $image,
        ]);
    }
    public function updateProduct($request, $id)
    {
        $product = static::find($id);
        $nameImage = $product->image;
        if($request->image) {
            $image = new Image;
            $nameImage = $image->updateImage($request->image, self::folder , str::slug($request->name), $nameImage);
        }
        $product->update([
            'name' => $request->name,
            'slug' => str::slug($request->name),
            'code' => $request->code,
            'category_id' => $request ->category_id,
            'status' => $request ->status,
            'note' => $request ->note,
            'image' => $nameImage
        ]);
    }   
    public function updateStatus($product_id, $status)
    {
        static::where('id', $product_id)->update([
            'status' => $status,
        ]);
    }

    
    public function deleteProduct($id)
    {
       if ($product= static::find($id)) {
            $image = new Image;
            $image->delImage(self::folder, $product->image);
            $product->delete();
            return true;
       }
       return false;
    }
}
