<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'user' => 'required|max:191|unique:users,user,'.request()->id,
            'email' => 'required|max:191|email:rfc|unique:users,email,'.request()->id,
            'department_id' => 'required|exists:departments,id',
            'gender' => 'required',
            'phone' => 'required|numeric',
            'type' => 'required',
            'address' => 'required',
            'role' => 'exists:roles,id'
        ];
    }
}
