<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191|unique:products,name,'.request()->id,
            'category_id'=> 'required',
            'code' =>'required|max:191',
            'status' => 'required',
            'note' => 'required',
            'image' => 'image'
        ];
    }
}
