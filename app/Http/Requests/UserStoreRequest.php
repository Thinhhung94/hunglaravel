<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function true()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'user' => 'required|max:191|unique:users,user',
            'email' => 'required|max:191|unique:users,email|email:rfc',
            'department_id' => 'required|exists:departments,id',
            'gender' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric',
            'type' => 'required',
            'role' => 'exists:roles,id'
        ];
    }
}
