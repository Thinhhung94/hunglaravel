<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Models\CategoryModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $category;
    public function __construct(CategoryModel $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $this->authorize('category.index');
        $categories = $this->category->list();
        return view('admin.categories.index',[
            'title' => 'List Category',
            'topTitle' => 'Danh sách Category ('.$categories->count().')',
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create',[
            'title' => 'Create Category',
            'topTitle' => 'Tạo Danh mục',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {
        $this->category->createCate($request);
        return redirect()->back()->with('success', 'Thêm thành công danh mục :'. $request->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->find($id);
        return view('admin.categories.update',[
            'title' => 'Edit Category',
            'topTitle' => 'Sửa danh mục',
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request,$id)
    {
        $this->category->updateCate($request, $id);
        return redirect(route('categories.index'))->with('success', 'Sửa thành công danh mục:'. $request->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $delete =  $this->category->deleteCategory($id);
        if($delete){
            return redirect()->back()->with('success', 'Đã xóa thành công');
        }
        return redirect()->back()->with('error', 'Có lỗi, Vui lòng liên hệ với quan trị viên');
    }
}
