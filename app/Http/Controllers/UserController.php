<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Models\DepartmentModel;
use App\Models\RoleMode;
use App\Models\RoleModel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $user;
    private $department;
    private $role;
    public function __construct(UserModel $user,DepartmentModel $department,RoleModel $role){
        $this->user = $user;
        $this->department = $department;
        $this->role = $role;
    }
    public function index()
    {
        $this->authorize('user.index');
        $users = $this->user->list();
        return view('admin.users.index',[
            'title' => 'Danh sách nhân viên',
            'topTitle' => 'Danh sách nhân viên ('.$users->count().')',
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('user.create');
        $departments = $this->department::all();
        $roles = $this->role->all();
        return view('admin.users.create', [
            'title' => 'Tạo tài khoản',
            'topTile' => 'Tạo tài khoản',
            'departments' => $departments,
            'roles' => $roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $this->authorize('user.create');
        $this->user->createUser($request);
        return redirect()->back()->with('success', 'Tạo thành công thành viên');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('user.update');
        $user = $this->user->find($id);
        $roles = $this->role->all();
        $departments = $this->department::all();
        return view('admin.users.Update', [
            'title' => 'Sửa tài khoản',
            'topTitle' => 'Sửa tài khoản : '.$user->name,
            'departments' => $departments,
            'user' => $user,
            'roles' => $roles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $this->authorize('user.update');
        $this->user->updateUser($request, $id);
        return redirect()->back()->with('success', 'Sửa thành công thành viên');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('user.delete');
        if( $this->user->deleteUser($id)){
            return redirect(route('users.index'))->with('success','Đã xóa thành công');
        }
        return redirect()->back()->with('error','có lỗi trong qá trình xóa vui lòng liên hệ với quản trị viên');
    }
    public function list()
    {
        $this->authorize('user.index');
        $users = $this->user->listUser();
        return view('admin.users.list', [
            'title' => 'Danh sách nhân viên',
            'users' => $users
        ]);

    }

    public function infomation($id)
    {
        $this->authorize('user.index');
        $user = $this->user->findById($id);
        return view('admin.users.infomation', [
            'title' => 'Chi tiết nhân viên : '. $user->name,
            'user' => $user
        ]);

    }
}
