<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleStoreRequest;
use App\Http\Requests\RoleUpdateRequest;
use App\Models\PermisstionModel;
use App\Models\RoleModel;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $role; 
    private $permisstion; 
    public function __construct(RoleModel $role, PermisstionModel $permisstion)
    {
        $this->role = $role;
        $this->permisstion = $permisstion;
    }
    public function index()
    {
        // $this->authorize('role.index');
        $roles= $this->role->list();
        return view('admin.roles.index',[
            'title' => 'Danh sách role',
            'roles' => $roles
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('role.create');
        $permisstions = $this->permisstion->list();
        return view('admin.roles.create',[
            'title' => 'Tạo role',
            'permisstions' => $permisstions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleStoreRequest $request)
    {
        $this->authorize('role.create');
        $this->role->createRole($request);
        return redirect()->back()->with('success','Thêm thành công role :'.$request->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $this->authorize('role.update');
        $role = $this->role->find($id);
        $permisstions = $this->permisstion->list();
        return view('admin.roles.update',[
            'title' => 'Edit role',
            'permisstions' => $permisstions,
            'role' => $role
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(RoleUpdateRequest $request,$id)
    {
        $this->authorize('role.update');
        $this->role->updateRole($request, $id);
        return redirect()->back()->with('success','Sửa thành công role :'.$request->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('role.delete');
        $this->role->deleteRole($id);
        return redirect()->back()->with('success','Xóa thành công role');
    }
}
