<?php

namespace App\Policies;

use App\Models\CategoryModel;
use App\Models\UserModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoriesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\UserModel  $userModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(UserModel $userModel)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\CategoryModel  $categoryModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(UserModel $userModel)
    {
        return $userModel->checkPemisstion('category_list');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\UserModel  $userModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(UserModel $userModel)
    {
        return $userModel->checkPemisstion('category_create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\CategoryModel  $categoryModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(UserModel $userModel)
    {
        return $userModel->checkPemisstion('category_update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\CategoryModel  $categoryModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(UserModel $userModel)
    {
        return $userModel->checkPemisstion('category_delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\CategoryModel  $categoryModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(UserModel $userModel, CategoryModel $categoryModel)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\CategoryModel  $categoryModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(UserModel $userModel, CategoryModel $categoryModel)
    {
        //
    }
}
