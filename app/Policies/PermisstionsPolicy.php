<?php

namespace App\Policies;

use App\Models\PermisstionModel;
use App\Models\UserModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermisstionsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\UserModel  $userModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(UserModel $userModel)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\PermisstionModel  $permisstionModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(UserModel $userModel)
    {
        return $userModel->checkPemisstion('permisstion_list');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\UserModel  $userModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(UserModel $userModel)
    {
        return $userModel->checkPemisstion('permisstion_create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\PermisstionModel  $permisstionModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(UserModel $userModel)
    {
        return $userModel->checkPemisstion('permisstion_update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\PermisstionModel  $permisstionModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(UserModel $userModel)
    {
        return $userModel->checkPemisstion('permisstion_delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\PermisstionModel  $permisstionModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(UserModel $userModel, PermisstionModel $permisstionModel)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\PermisstionModel  $permisstionModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(UserModel $userModel, PermisstionModel $permisstionModel)
    {
        //
    }
}
