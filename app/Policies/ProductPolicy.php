<?php

namespace App\Policies;

use App\Models\ProductModel;
use App\Models\UserModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\UserModel  $userModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(UserModel $userModel)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\ProductModel  $productModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(UserModel $userModel)
    {
        return $userModel->checkPemisstion('product_list');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\UserModel  $userModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(UserModel $userModel)
    {
       return $userModel->checkPemisstion('product_create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\ProductModel  $productModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(UserModel $userModel)
    {
        return $userModel->checkPemisstion('product_update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\ProductModel  $productModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(UserModel $userModel)
    {
        return $userModel->checkPemisstion('product_delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\ProductModel  $productModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(UserModel $userModel, ProductModel $productModel)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\UserModel  $userModel
     * @param  \App\Models\ProductModel  $productModel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(UserModel $userModel, ProductModel $productModel)
    {
        //
    }
}
