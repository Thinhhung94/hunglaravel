<?php
// product table status

define("first_login", 0);
define("warehouse", 0);
define("borrow", 1);

// User table type
define("typeUser", 1);
define("typeAdmin", 0);

// User table gender
define("genderMafe", 0);
define("genderFemale", 1);

//Request table status 
define("requestPending", 0);
define("requestSolved", 1);
define("requestRefuse", 2);
define("returned", 3);

// Request table action
define("requestDefault", 0);
define("requestExport", 1);
define("requestExported", 2);
define("requestReturn", 3);

// history_request table  status
define("historyRequestDefault", 0);
define("historyRequestAgree", 1);
define("historyRequestDeny", 2);
define("historyRequestReturn", 3);
